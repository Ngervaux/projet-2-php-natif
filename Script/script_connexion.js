//Récupération du formulaire :
var form = document.getElementById("formConnexion");

// Récupération des boutons :
var btnConnexion = document.getElementById("btnConnexion");


// Abonnements :
btnConnexion.addEventListener("click", controleChampConnexion);

//Contrôle des champs du formulaire connexion :


/**
 * Si le nom d'utilisateur ou le mot de passe est vide, affichez un message d'alerte. Sinon, envoyez le
 * formulaire.
 */
function controleChampConnexion(){
    var user= document.getElementById("userName").value;
    var password = document.getElementById("password").value;

    if(user.trim() == "" || password.trim() == ""){
        alert("Un des champs est vide");
    } else {
        form.submit();
    }

}