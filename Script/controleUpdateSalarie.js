//Les regex :
var controleNom = /^[a-zA-Z\u00C0-\u00FF\s\'\-]*$/;
var controleUtilisateur = /([^a-zA-Z0-9âêîôûàèùòÿäëïöüÿéãñõ])/

//Récupération du formulaire :
var form = document.getElementById("formSalarie");

// Récupération des boutons :
var btnUpdate = document.getElementById("btnUpdate");


// Abonnements :
btnUpdate.addEventListener("click", controleChampAjout);

//Contrôle des champs du formulaire ajout :

/* Une fonction qui est appelée lorsque le bouton est cliqué. Il vérifie si les champs sont vides et
s'ils ne sont pas vides, il vérifie si les champs sont valides. Si les champs sont valides, il
soumet le formulaire. */
function controleChampAjout(){
    var nom= document.getElementById("nom").value;
    var prenom= document.getElementById("prenom").value;

    if(nom.trim() == "" || prenom.trim() == ""){
        alert("Un des champs est vide");
    }
    else if(!controleNom.test(nom) || !controleNom.test(prenom)){
        alert("Non respect des caractères champ nom/prenom");
    }
    else {
        form.submit();
    }

}
