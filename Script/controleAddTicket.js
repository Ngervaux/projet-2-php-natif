// regex

// var regNumber = /^[0-9][A-Za-z0-9 -]*$/;
var regNumber = /^[0-9]/;
// var regEspace = /^[a-zA-Z,\d,\-,\(,\']{1,5}$/;
var regEspace = /\s\s/
var regNoEspace = /\s/;

// variables 

var bntAdd = document.getElementById("addTicket");

// recuperation du formulaire

var form = document.getElementById("formulaire");

// abonnement

bntAdd.addEventListener("click", controleFormTicket);

// fonction

function controleFormTicket(){

    var motifTicket = document.getElementById("motifTicket").value;
    var numCommande = document.getElementById("numCommande").value;
    var numDossier = document.getElementById("numDossier").value;

    if(motifTicket.trim() == "" || numCommande.trim() == "" || numDossier.trim() == ""){
        alert("Un champs des champs est vide veuillez tous les completer");
    }
    else if(!regNumber.test(numCommande) || !regNumber.test(numDossier)){
        alert("Les champs numero de dossier et numero de commande doivent contenir des nombres");
    }
    else if(regEspace.test(motifTicket)){
        alert("un seul espace entre les mots ou les differents caracteres est autorisé")
    }
    else if(regNoEspace.test(numCommande) || regNoEspace.test(numDossier)){
        alert("Les espaces ne sont pas autorisé dans les champs numero de commande et numero de dossier");
    }
    else{
        form.submit();
    }

}
