<?php
session_start();
// require :
require('../sav/Modele/DbSav.class.php');
require('../sav/Modele/connexion.class.php');
require('../sav/Modele/Admin.class.php');
require('./Modele/Salarie.class.php');
require('../sav/Modele/TicketSAV.class.php');
require('../sav/Modele/ManagerTicketSAV.class.php');
require("./Modele/listeclient.class.php");
require("./Modele/detailsCommande.class.php");
require("./Modele/commande.class.php");

//Initiation des variables :
$msg = '';
$tResult = [];
$nblien = -1;
$flag = -1;

//Si aucune action en Session, mets sur l'écran de connexion :
if(!isset($_SESSION['postdata']['action'])){
    $_SESSION['postdata']['action'] ="connexion";
}
//Récupère $_POST pour le mettre en session, évite de renvoyer formulaire lors d'un F5 :
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $_SESSION['postdata'] = $_POST;
    unset($_POST);
    header("Location: ".$_SERVER['PHP_SELF']);
    exit;
}


//Le switch :

switch ($_SESSION['postdata']['action']) {
    case 'accueil':
        if ($_SESSION['role'] == 'Admin') {
            $tSalarie = Admin::getListeSalaries();
            require('./Vues/Vue_header.php');
            require('./Vues/vue_adminAccueil.php');
            require('./Vues/Vue_footer.php');
            break;
        }
        elseif ($_SESSION['role'] == 'Hotline'){
            require("./Vues/Vue_header.php");
            require("./Vues/Vue_hotline.php");
            require("./Vues/Vue_footer.php");
            break;
        }
        elseif ($_SESSION['role'] == 'Technicien'){
            require("./Vues/Vue_header.php");
            require("./Vues/vue_technicien.php");
            require("./Vues/Vue_footer.php");
            break;
        }
        
    case 'connexion':
        $msg = 'Veuillez vous connecter';
        require('./Vues/Vue_PageConnexion.php');
        break;
    case 'connexionMAJ':
        $user = $_SESSION['postdata']['userName'];
        $password = $_SESSION['postdata']['password'];
        try {
        $tResult = Connexion::VerifConnexion($user, $password);
        } catch (Exception $msg) {
            $msg = "Erreur login ou mot de passe";
        }

        if (empty($tResult)) {
            require('./Vues/Vue_PageConnexion.php');
            break;
        } else {
            $_SESSION['id'] = $tResult[0]['IdSalarie'];
            $_SESSION['nom'] = $tResult[0]['nomSalarie'];
            $_SESSION['prenom'] = $tResult[0]['prenomSalarie'];
            $_SESSION['role'] = $tResult[0]['roleSalarie'];
            $_SESSION['postdata']['action'] == 'accueil';
            $id =  $_SESSION['id'];
            $nom =  $_SESSION['nom'];
            $prenom = $_SESSION['prenom'];
            $role = $_SESSION['role'];
            if ($_SESSION['role'] == 'Admin') {
                $tSalarie = Admin::getListeSalaries();
                require('./Vues/Vue_header.php');
                require('./Vues/vue_adminAccueil.php');
                require('./Vues/Vue_footer.php');
                break;
            }
            elseif ($_SESSION['role'] == 'Hotline'){
                require("./Vues/Vue_header.php");
                require("./Vues/Vue_hotline.php");
                require("./Vues/Vue_footer.php");
                break;
            }
            elseif ($_SESSION['role'] == 'Technicien'){
                require("./Vues/Vue_header.php");
                require("./Vues/vue_technicien.php");
                require("./Vues/Vue_footer.php");
                break;
            }
            elseif (($_SESSION['role'] == 'Archive')){
                $msg = 'Vous ne faites plus partie des effectifs';
                require('./Vues/Vue_PageConnexion.php');
                break;
            }
        }
    case 'deconnexion':
        session_unset();
        $_POST = array();
        DbSavClass::disconnect();
        $msg = 'Vous êtes déconnecté';
        header('Location: index.php');
        break;
    case 'deleteSalarie':
        $idSalarie = $_SESSION['postdata']['idSalarie'];
        $nomSalarie = $_SESSION['postdata']['nomSalarie'];
        $prenomSalarie = $_SESSION['postdata']['prenomSalarie'];
        $_POST=array();
        $tnblien = Admin::verifLienSalarieTicket($idSalarie);
        $nblien = $tnblien[0];
        $_SESSION['action'] = 'accueil';
        if ($nblien == 0) {
            $nb = Admin::delSalarieById($idSalarie);

            if ($nb == 1) {
                $msg = 'Le salarié ' . $nomSalarie . ' est supprimé.';
                $flag = 0;
                $tSalarie = Admin::getListeSalaries();
                require('./Vues/Vue_header.php');
                require('./Vues/vue_adminAccueil.php');
                require('./Vues/Vue_footer.php');
                break;
            } else {
                $msg = 'Erreur lors de la suppression du salarié';
                $flag = 1;
                header('Location: index.php');
                exit;
            }
        } else {
            $roleSalarie = 'Archive';
            $nb = Admin::updateSalarieById($idSalarie, $nomSalarie, $prenomSalarie, $roleSalarie);
            if ($nb == 1) {
                $msg = 'Le salarié ' . $nomSalarie . ' est archivé.';
                $tSalarie = Admin::getListeSalaries();
                $flag = 0;
                require('./Vues/Vue_header.php');
                require('./Vues/vue_adminAccueil.php');
                require('./Vues/Vue_footer.php');
                break;
            } else {
                $msg = 'Erreur lors de la tentative de mise en archive du salarié.';
                $flag = 1;
                header('Location: index.php');
                exit;
            }
        }
    case 'updateSalarie':
        $idSalarie = $_SESSION['postdata']['idSalarie'];
        $nomSalarie = $_SESSION['postdata']['nomSalarie'];
        $prenomSalarie = $_SESSION['postdata']['prenomSalarie'];
        $roleSalarie = $_SESSION['postdata']['roleSalarie'];
        require('./Vues/Vue_header.php');
        require('./Vues/vue.formulaireSalarie.php');
        require('./Vues/Vue_footer.php');
        break;
    case 'updateSalarieMAJ':
        $nomSalarie = $_SESSION['postdata']['nomSalarie'];
        $prenomSalarie = $_SESSION['postdata']['prenomSalarie'];
        $roleSalarie = $_SESSION['postdata']['roleSalarie'];
        $idSalarie = $_SESSION['postdata']['idSalarie'];
        $nb = Admin::updateSalarieById($idSalarie, $nomSalarie, $prenomSalarie, $roleSalarie);
        if ($nb == 1) {
            $msg = 'Le salarié ' . $nomSalarie . ' ' . $prenomSalarie . ' a bien était mis à jour. Rôle : ' . $roleSalarie . '.';
            $flag = 0;
            $tSalarie = Admin::getListeSalaries();
            $_SESSION['postdata']['action'] = 'accueil';
            require('./Vues/Vue_header.php');
            require('./Vues/vue_adminAccueil.php');
            require('./Vues/Vue_footer.php');
            break;
        } else {
            $msg = 'Erreur lors de la tentative de modification du salarié : aucune modification';
            $tSalarie = Admin::getListeSalaries();
            $flag = 1;
            require('./Vues/Vue_header.php');
            require('./Vues/vue_adminAccueil.php');
            require('./Vues/Vue_footer.php');
            break;
        }
    case 'addSalarie':
        $idSalarie = "";
        $nomSalarie = "";
        $prenomSalarie = "";
        $roleSalarie = "";
        $utilSalarie="";
        $mdpSalarie="";
        require('./Vues/Vue_header.php');
        require('./Vues/vue.formulaireSalarie.php');
        require('./Vues/Vue_footer.php');
        break;
    case 'addSalarieMAJ':
        $nomSalarie = $_SESSION['postdata']['nomSalarie'];
        $prenomSalarie = $_SESSION['postdata']['prenomSalarie'];
        $roleSalarie = $_SESSION['postdata']['roleSalarie'];
        $utilSalarie = $_SESSION['postdata']['utilSalarie'];
        $mdpSalarie = password_hash($_SESSION['postdata']['mdpSalarie'], PASSWORD_DEFAULT);
        $nb = Admin::verifUtilisateurDansTable($utilSalarie);

        $salarie1 = new Salarie(0, $nomSalarie, $prenomSalarie, $roleSalarie, $utilSalarie, $mdpSalarie);
        if ($nb == 0) {
            Admin::addSalarie($salarie1);
            $flag = 0;
            $msg = 'Le salarié ' . $nomSalarie . ' ' . $prenomSalarie . ' a bien était crée. Rôle : ' . $roleSalarie . '.';
            $_POST=array();
            $_SESSION['postdata']["action"] = 'accueil';
            $tSalarie = Admin::getListeSalaries();
            require('./Vues/Vue_header.php');
            require('./Vues/vue_adminAccueil.php');
            require('./Vues/Vue_footer.php');
            break;
        } else {
            $flag = 2;
            $msg = 'Identifiant utilisateur déjà présent en BDD';
            $nomSalarie = $_SESSION['postdata']['nomSalarie'];
            $prenomSalarie = $_SESSION['postdata']['prenomSalarie'];
            $utilSalarie = $_SESSION['postdata']['utilSalarie'];
            $mdpSalarie = $_SESSION['postdata']['mdpSalarie'];

            require('./Vues/Vue_header.php');
            require('./Vues/vue.formulaireSalarie.php');
            require('./Vues/Vue_footer.php');
            break;
        }
    case 'listeCommande':
        $row = ListeCommandes::getListCommandes();
        require('Vues/vue_header.php');
        require("./Vues/vue_commande.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'listeTickets':
        $tListeTicket = ManagerTicketSAV::getListTicket();
        require('Vues/vue_header.php');
        require('Vues/vue_listeTicket.php');
        require('Vues/vue_footer.php');
        break;

    case 'listeCommandeTech':
        $row = ListeCommandes::getListCommandes();
        require('Vues/vue_header.php');
        require("./Vues/vue_commandeTech.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'listeClient':
        $tVille = ListeClient::getVilleClient();
        require("./Vues/Vue_header.php");
        require("./Vues/vue_listeclient.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'listeClientTech':
        $tVille = ListeClient::getVilleClient();
        require("./Vues/Vue_header.php");
        require("./Vues/vue_listeclientTech.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'AfficherCommandes':
        $id = $_SESSION['postdata']['idClient'];
        $row = ListeClient::getListClient($id);
        require("./Vues/Vue_header.php");
        require("./Vues/vue_listeCommandes.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'AfficherCommandesTech':
        $id = $_SESSION['postdata']['idClient'];
        $row = ListeClient::getListClient($id);
        require("./Vues/Vue_header.php");
        require("./Vues/vue_listeCommandesTech.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'AfficherDetailsCommande':
        $idnumCommande = $_SESSION['postdata']['numCommande'];
        $tCommande = DetailsCommandes::getDetailsCommande($idnumCommande);
        require("./Vues/Vue_header.php");
        require("./Vues/vue_detailscommande.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'AfficherDetailsCommandeTech':
        $idnumCommande = $_SESSION['postdata']['numCommande'];
        // $idProduit = $_POST['refProduit'];
        $tCommande = DetailsCommandes::getDetailsCommande($idnumCommande);
        // $tProduit = DetailsCommandes::getNomproduit($idProduit);
        require("./Vues/Vue_header.php");
        require("./Vues/vue_detailscommandeTech.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'Recherche':
        require("./Vues/Vue_header.php");
        require("./Vues/vu_recherche.php");
        require("./Vues/Vue_footer.php");
        break;

    case 'CreationTicket':
        $motifTicket ="";
        $numCommande = $_SESSION['postdata']['numCommande'];
        $numDossier = "";
        require('Vues/vue_header.php');
        require('Vues/vue_formulaire.php');
        require('Vues/vue_footer.php');
        break;
    case 'CreationTicketMaj':
        $motifTicket = $_SESSION['postdata']['motifTicket'];
        $numCommande = $_SESSION['postdata']['numCommande'];
        $numDossier = $_SESSION['postdata']['numDossier'];
        $dateTicket = date('Y-m-d',time());
        $nbTest = ManagerTicketSAV::checkNumCommande($numCommande);
        If($nbTest == 1) {
            $ticket = new Ticket($dateTicket, $motifTicket, $numCommande, $numDossier);
            $idSalarie = intval($_SESSION['id']);
            $newTicket = ManagerTicketSAV::addTicket($ticket);
            ManagerTicketSAV::ajoutGerer($idSalarie);
            $tListeTicket = ManagerTicketSAV::getListTicket();
            $msg = "Ticket crée !";
            $_SESSION['postdata']['action'] ="listeTickets";
            require('Vues/vue_header.php');
            require('Vues/vue_listeTicket.php');
            require('Vues/vue_footer.php');
            break;
        }
        else{
            $msg = "Le numéro de commande est inconnu";
            require('Vues/vue_header.php');
            require('Vues/vue_formulaire.php');
            require('Vues/vue_footer.php');
            break;
        }
       
    case 'infoTicket':
        $idTicket = $_SESSION['postdata']['idTicket'];
        $idTicket = intval($idTicket);
        $tListeTicket = ManagerTicketSAV::getInfoTicket($idTicket);
        require('Vues/vue_header.php');
        require('Vues/vue_infoTicket.php');
        require('Vues/vue_footer.php');
        break;
    }
    

