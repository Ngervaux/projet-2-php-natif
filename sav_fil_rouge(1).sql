-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : dim. 08 mai 2022 à 16:11
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sav fil rouge`
--
CREATE DATABASE IF NOT EXISTS `sav fil rouge` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sav fil rouge`;

-- --------------------------------------------------------

--
-- Structure de la table `adresse_livraison`
--

DROP TABLE IF EXISTS `adresse_livraison`;
CREATE TABLE IF NOT EXISTS `adresse_livraison` (
  `idLivraison` int(11) NOT NULL,
  `adresseLivraison` varchar(50) DEFAULT NULL,
  `cpLivraison` int(11) DEFAULT NULL,
  `idVille` int(11) DEFAULT NULL,
  `idClient` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLivraison`),
  KEY `idVille` (`idVille`),
  KEY `idClient` (`idClient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `adresse_livraison`
--

INSERT INTO `adresse_livraison` (`idLivraison`, `adresseLivraison`, `cpLivraison`, `idVille`, `idClient`) VALUES
(1, '2 rue de la misericorde', 14000, 1, 1),
(2, '56 rue du manque d\'inspiration', 50000, 2, 3),
(3, '25 rue du test', 14123, 4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `codeArticle` int(11) NOT NULL,
  `libelleArticle` varchar(50) DEFAULT NULL,
  `prixArticle` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codeArticle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`codeArticle`, `libelleArticle`, `prixArticle`) VALUES
(1, 'vis cruciforme', '1.00'),
(2, 'Télécommande universelle', '25.00'),
(3, 'rail pour portail', '30.00'),
(4, 'moteur portail', '350.00'),
(5, 'détecteur portail', '150.00');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `idClient` int(11) NOT NULL AUTO_INCREMENT,
  `nomClient` varchar(50) DEFAULT NULL,
  `prenomClient` varchar(50) DEFAULT NULL,
  `adresseClient` varchar(50) DEFAULT NULL,
  `idVille` int(11) DEFAULT NULL,
  PRIMARY KEY (`idClient`),
  KEY `idVille` (`idVille`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`idClient`, `nomClient`, `prenomClient`, `adresseClient`, `idVille`) VALUES
(1, 'LOREM', 'Ipsum', '33 rue de l\'inconnue', 1),
(2, 'NORRIS', 'Chuck', '20 allée des champions', 4),
(3, 'ALESI', 'Jean', '110 rue de la vitesse', 2),
(4, 'PROST', 'Alain', '100 Rue de la vitesse', 2);

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `numCommande` int(11) NOT NULL,
  `dateCommande` date DEFAULT NULL,
  `etatCommande` varchar(50) DEFAULT NULL,
  `numFacture` int(11) DEFAULT NULL,
  `idClient` int(11) DEFAULT NULL,
  PRIMARY KEY (`numCommande`),
  UNIQUE KEY `numFacture` (`numFacture`),
  KEY `idClient` (`idClient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`numCommande`, `dateCommande`, `etatCommande`, `numFacture`, `idClient`) VALUES
(1010, '2022-05-04', 'Livraison annulée', 4321, 1),
(1212, '2022-05-04', 'En cours de préparation', 2468, 2),
(2345, '2022-05-04', 'Livrée', 1234, 1),
(2468, '2022-05-05', 'En cours de préparation', 2112, 1),
(5678, '2022-05-04', 'En cours de livraison', 1357, 3),
(6824, '2022-05-05', 'En cours de livraison', 2106, 3),
(9876, '2022-05-05', 'Livraison annulée', 1405, 2),
(10001, '2022-02-02', 'En attente de paiement', 10002, 1),
(10002, '2022-01-13', 'En attente de paiement', 10003, 2),
(10003, '2022-03-31', 'En attente de paiement', 10004, 1),
(10004, '2022-04-21', 'En attente de paiement', 10005, 3);

-- --------------------------------------------------------

--
-- Structure de la table `composer`
--

DROP TABLE IF EXISTS `composer`;
CREATE TABLE IF NOT EXISTS `composer` (
  `refProduit` varchar(50) NOT NULL,
  `codeArticle` int(11) NOT NULL,
  `quantiteArticle` int(11) DEFAULT NULL,
  PRIMARY KEY (`refProduit`,`codeArticle`),
  KEY `codeArticle` (`codeArticle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `comprendre`
--

DROP TABLE IF EXISTS `comprendre`;
CREATE TABLE IF NOT EXISTS `comprendre` (
  `refProduit` varchar(50) NOT NULL,
  `numCommande` int(11) NOT NULL,
  `quantiteProduit` int(11) DEFAULT NULL,
  PRIMARY KEY (`refProduit`,`numCommande`),
  KEY `numCommande` (`numCommande`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comprendre`
--

INSERT INTO `comprendre` (`refProduit`, `numCommande`, `quantiteProduit`) VALUES
('101001', 10001, 50),
('101001', 10004, 50),
('101002', 10001, 150);

-- --------------------------------------------------------

--
-- Structure de la table `diagnostique`
--

DROP TABLE IF EXISTS `diagnostique`;
CREATE TABLE IF NOT EXISTS `diagnostique` (
  `idDiagnostique` int(11) NOT NULL,
  `blabla` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idDiagnostique`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `dossier`
--

DROP TABLE IF EXISTS `dossier`;
CREATE TABLE IF NOT EXISTS `dossier` (
  `numDossier` int(11) NOT NULL,
  `typeDossier` varchar(4) DEFAULT NULL,
  `dateOuverDossier` date DEFAULT NULL,
  `dateFinDossier` date DEFAULT NULL,
  `etatDossier` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`numDossier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `dossier`
--

INSERT INTO `dossier` (`numDossier`, `typeDossier`, `dateOuverDossier`, `dateFinDossier`, `etatDossier`) VALUES
(1001, 'NPAI', '2022-05-04', '2022-05-04', 'en cours de traitement'),
(1010, 'NP', '2022-05-04', '2022-05-04', 'pas encore traiter'),
(1020, 'EC', '2022-05-04', '2022-05-04', 'Traitement du dossier terminer');

-- --------------------------------------------------------

--
-- Structure de la table `expedition`
--

DROP TABLE IF EXISTS `expedition`;
CREATE TABLE IF NOT EXISTS `expedition` (
  `idExpedition` int(11) NOT NULL,
  `dateExpedition` date DEFAULT NULL,
  `idTicket` int(11) DEFAULT NULL,
  `numCommande` int(11) DEFAULT NULL,
  PRIMARY KEY (`idExpedition`),
  KEY `idTicket` (`idTicket`),
  KEY `numCommande` (`numCommande`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

DROP TABLE IF EXISTS `facture`;
CREATE TABLE IF NOT EXISTS `facture` (
  `numFacture` int(11) NOT NULL,
  `dateFacture` date DEFAULT NULL,
  PRIMARY KEY (`numFacture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `facture`
--

INSERT INTO `facture` (`numFacture`, `dateFacture`) VALUES
(1234, '2022-05-04'),
(1357, '2022-05-04'),
(1405, '2022-05-05'),
(2106, '2022-05-05'),
(2112, '2022-05-05'),
(2468, '2022-05-04');

-- --------------------------------------------------------

--
-- Structure de la table `garantie`
--

DROP TABLE IF EXISTS `garantie`;
CREATE TABLE IF NOT EXISTS `garantie` (
  `idGarantie` int(11) NOT NULL,
  `dureeGarantie` int(11) DEFAULT NULL,
  `typeGarantie` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idGarantie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `garantie`
--

INSERT INTO `garantie` (`idGarantie`, `dureeGarantie`, `typeGarantie`) VALUES
(110, 10, 'Main-d\'oeuvre non inclu'),
(215, 15, 'Main-d\'oeuvre comprise'),
(316, 16, 'Tout compris');



-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `refProduit` varchar(50) NOT NULL,
  `prixProduit` decimal(10,2) DEFAULT NULL,
  `nomProduit` varchar(50) DEFAULT NULL,
  `idGarantie` int(10) DEFAULT NULL,
  PRIMARY KEY (`refProduit`),
  KEY `idGarantie` (`idGarantie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`refProduit`, `prixProduit`, `nomProduit`, `idGarantie`) VALUES
('101001', '179.00', 'Clôture alu ATHENES', 10),
('101002', '193.00', 'Clôture aluminium NAUPLIE', 5),
('101003', '199.00', 'Clôture aluminium OLYMPIE', 20),
('102001', '2232.00', 'Kit Grillage Rigie Gamme PRO', 15);

-- --------------------------------------------------------

--
-- Structure de la table `salarie`
--

DROP TABLE IF EXISTS `salarie`;
CREATE TABLE IF NOT EXISTS `salarie` (
  `idSalarie` int(11) NOT NULL,
  `nomSalarie` varchar(50) DEFAULT NULL,
  `prenomSalarie` varchar(50) DEFAULT NULL,
  `roleSalarie` varchar(50) DEFAULT NULL,
  `utilisateurSalarie` varchar(50) DEFAULT NULL,
  `motdepasseSalarie` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idSalarie`),
  UNIQUE KEY `utilisateurSalarie` (`utilisateurSalarie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salarie`
--

INSERT INTO `salarie` (`idSalarie`, `nomSalarie`, `prenomSalarie`, `roleSalarie`, `utilisateurSalarie`, `motdepasseSalarie`) VALUES
(1, 'CHIRAC', 'Jacques', 'Technicien', 'test1', '$2y$10$.8JKBfB1W4UdfB81NgUl/eZrZQVUd.VgdDZzcGkTRJvcxIm8Lu.KK'),
(2, 'HOLLANDE', 'François', 'Technicien', 'test2', '$2y$10$Id1Tn2iGzsIrAhDFRVuCKe9zBNMvyl.glOs29hmGIpc1dPCHr8p1G'),
(3, 'SARKOZY', 'Nicolas', 'Hotline', 'test3', '$2y$10$e0lo4ctVb.YD2q5brwFhDOf.GCWYqJ71bngbwigc8FyIYglsqLG3q'),
(4, 'MACRON', 'Emmanuel', 'Hotline', 'test4', '$2y$10$1emCAZDkdPWf78FtWjRT6uuISjZ47ssSCgPfLTdWjxy1XY6ssNLcm'),
(5, 'LENINE', 'Vladimir Illitch', 'Admin', 'test5', '$2y$10$.8JKBfB1W4UdfB81NgUl/eZrZQVUd.VgdDZzcGkTRJvcxIm8Lu.KK');

--
-- Déclencheurs `salarie`
--
DROP TRIGGER IF EXISTS `insert_salarie`;
DELIMITER $$
CREATE TRIGGER `insert_salarie` BEFORE INSERT ON `salarie` FOR EACH ROW BEGIN
	DECLARE maxId INT;
    SELECT MAX(idSalarie) INTO maxID FROM salarie;
    
    SET new.idSalarie := maxID+1;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `soumettre`
--

DROP TABLE IF EXISTS `soumettre`;
CREATE TABLE IF NOT EXISTS `soumettre` (
  `codeArticle` int(11) NOT NULL,
  `idTicket` int(11) NOT NULL,
  `probleme` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codeArticle`,`idTicket`),
  KEY `idTicket` (`idTicket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `stocker_article`
--

DROP TABLE IF EXISTS `stocker_article`;
CREATE TABLE IF NOT EXISTS `stocker_article` (
  `codeArticle` int(11) NOT NULL,
  `nomStock` varchar(50) NOT NULL,
  `quantiteArticleStock` int(11) DEFAULT NULL,
  PRIMARY KEY (`codeArticle`,`nomStock`),
  KEY `nomStock` (`nomStock`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `stocker_produit`
--

DROP TABLE IF EXISTS `stocker_produit`;
CREATE TABLE IF NOT EXISTS `stocker_produit` (
  `refProduit` varchar(50) NOT NULL,
  `nomStock` varchar(50) NOT NULL,
  `quantiteProduitStock` int(11) DEFAULT NULL,
  PRIMARY KEY (`refProduit`,`nomStock`),
  KEY `nomStock` (`nomStock`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `stock_general`
--

DROP TABLE IF EXISTS `stock_general`;
CREATE TABLE IF NOT EXISTS `stock_general` (
  `nomStock` varchar(50) NOT NULL,
  PRIMARY KEY (`nomStock`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ticket_sav`
--

DROP TABLE IF EXISTS `ticket_sav`;
CREATE TABLE IF NOT EXISTS `ticket_sav` (
  `idTicket` int(11) NOT NULL AUTO_INCREMENT,
  `dateTicket` date DEFAULT NULL,
  `motifTicket` varchar(50) DEFAULT NULL,
  `idDiagnostique` int(11) DEFAULT NULL,
  `numCommande` int(11) DEFAULT NULL,
  `numDossier` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTicket`),
  KEY `idDiagnostique` (`idDiagnostique`),
  KEY `numCommande` (`numCommande`),
  KEY `numDossier` (`numDossier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ticket_sav`
--

INSERT INTO `ticket_sav` (`idTicket`, `dateTicket`, `motifTicket`, `idDiagnostique`, `numCommande`, `numDossier`) VALUES
(1, '2022-05-04', 'panne moteur', NULL, 2345, 1001),
(2, '2022-05-04', 'manque piece', NULL, 1212, 1010),
(3, '2022-05-04', 'manque piece', NULL, 5678, 1020),
(4, '2022-05-04', 'sur-tension', NULL, 1010, 1020),
(5, '2022-05-05', 'article en panne', NULL, 2345, 1001);

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

DROP TABLE IF EXISTS `ville`;
CREATE TABLE IF NOT EXISTS `ville` (
  `idVille` int(11) NOT NULL AUTO_INCREMENT,
  `nomVille` varchar(50) DEFAULT NULL,
  `cpVille` int(6) DEFAULT NULL,
  PRIMARY KEY (`idVille`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`idVille`, `nomVille`, `cpVille`) VALUES
(1, 'Caen', 14000),
(2, 'Saint-Lô', 50000),
(3, 'Cherbourg', 50100),
(4, 'Ifs', 14123);
COMMIT;

-- --------------------------------------------------------

--
-- Structure de la table `gerer`
--

DROP TABLE IF EXISTS `gerer`;
CREATE TABLE IF NOT EXISTS `gerer` (
  `idTicket` int(11) NOT NULL,
  `idSalarie` int(11) NOT NULL,
  PRIMARY KEY (`idTicket`,`idSalarie`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `gerer`
--

INSERT INTO `gerer` (`idTicket`, `idSalarie`) VALUES
(1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
