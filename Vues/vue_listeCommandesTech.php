

 <h1 class="text-center">Liste des Commandes</h1>
       <table class="table">
    <thead>
      <tr>

        <th scope="col">Date de la Commande</th>
        <th scope="col">État de la Commande</th>
        <th scope="col">Numéro de facture</th>
        <th scope="col">Numéro de client</th>
        <th scope="col">Nom du Client</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
    <?php 
    for ($i = 0; $i < count($row); $i++)  {
        ?>
        <tr>
          <td><?php echo htmlspecialchars($row[$i]['dateCommande']); ?></td>
          <td><?php echo htmlspecialchars($row[$i]['etatCommande']); ?></td>
          <td><?php echo htmlspecialchars($row[$i]['numFacture']); ?></td>
          <td><?php echo htmlspecialchars($row[$i]['idClient']); ?></td>
          <td><?php echo htmlspecialchars($row[$i]['nomClient']); ?></td>
          <form method="post" action="">
            <td><button type="submit" class="btn btn-success">Afficher détails commande</button></td>
            <input type="hidden" name="action" value="AfficherDetailsCommandeTech">
            <input type="hidden" name="numCommande" value="<?php echo $row[$i]['numCommande'] ?>">
            
          </form>
        </tr>
       <?php } ?>
    </tbody>
  </table>
  <div class="text-center">
  <?php if (count($row) < 1) {
        echo "Aucune commande trouvée";
    }?>
  </div>


