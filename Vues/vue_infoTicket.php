<div class="infoTicket">

    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h1 class="card-title">Information sur le ticket N° : <?php echo $idTicket ?></h1>
            <?php foreach ($tListeTicket as $key=>$value) { ?>
                <tr>
                    <p class="card-text">Numero du ticket :<?php echo $value["idTicket"]; ?></p>
                    <p class="card-text">Client : <?php echo $value["nomClient"]; ?> <?php echo $value["prenomClient"]; ?></p>
                    <p class="card-text">Date du ticket : <?php echo $value["dateTicket"];?></p>
                    <p class="card-text">Motif du ticket : <?php echo $value["motifTicket"]; ?></p>
                    <p class="card-text">Numero de commande : <?php echo $value["numCommande"]; ?></p>
                    <p class="card-text">Numero de dossier : <?php echo $value["numDossier"]; ?></p>
            <?php }
            ?>
        </div>
    </div>
</div>
