<?php
if(strlen($msg)>0){  ?>
 
    <div class="alert alert-danger text-center" role="alert">
    <?php echo $msg ?>
</div> <?php } ?>

<form class="text-center" action="index.php" method="post" id="formSalarie">
<div class="form-row ">
    <div class="col-12 my-1">
        <label class="sr-only" for="inlineFormInputName">Nom du salarié</label>
        <div class="d-flex justify-content-center"><input type="text" id="nom" class="form-control w-50" name='nomSalarie' value='<?php echo $nomSalarie ?>'></div>
    </div>
    <div class="col-12 my-1">
        <label class="sr-only" for="inlineFormInputName">Prénom du salarié</label>
        <div class="d-flex justify-content-center">
        <input type="text" class="form-control w-50" id="prenom" name='prenomSalarie' value='<?php echo $prenomSalarie ?>'>
        </div>
    </div>
    <div class="col-12 my-1">
        <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect" >Rôle</label>
        <div class="d-flex justify-content-center">
        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="roleSalarie">
            <option value="Technicien">Technicien</option>
            <option value="Hotline">Hotline</option>
            <option value="Admin">Administrateur</option>
        </select>
    </div>
    </div>
    <?php if ($_SESSION['postdata']['action'] == "addSalarie" || $flag ==2) { ?>


        <div class="col-12 my-1">
        <label class="sr-only" for="inlineFormInputName">Identifiant connexion du salarié :</label>
        <div class="d-flex justify-content-center"><input type="text" id="identifiant" class="form-control w-50" name='utilSalarie' value='<?php echo $utilSalarie ?>'></div>
    </div>
    <div class="col-12 my-1">
        <label class="sr-only" for="inlineFormInputName">Mot de passe du salarié :</label>
        <div class="d-flex justify-content-center">
        <input type="text" id="motdepasse" class="form-control w-50" name='mdpSalarie' value="<?php echo $mdpSalarie ?> ">
        </div>
    </div>
    <div class="form-group">
            <button type="button" id="btnAdd" class="btn btn-success">Ajouter salarié</button>
            <input type="hidden" name="action" value="addSalarieMAJ">
            
        </div>

    <?php } else { ?>

    <div class="form-group">
            <button type="button" id="btnUpdate" class="btn btn-success">Modifier</button>
            <input type="hidden" name="action" value="updateSalarieMAJ">
            <input type="hidden" name="idSalarie" value="<?php echo $idSalarie; ?>" />
        </div>
        <?php } ?>
</div>
</form>

<!-- <script src="../controlFormTicketCrea.js"></script> -->

</body>
</body>
<script src="../Script/controleAddSalarie.js"></script>
<script src="../Script/controleUpdateSalarie.js"></script>
</html>