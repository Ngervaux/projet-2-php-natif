<body>
    <footer>
        <div class="container-fluid fixed-bottom">
            <div class="row footer">
                
                    <div class="text-start col-12 col-sm-4 col-md-4 col-lg-4">
                        <span>SARL MENUIZ MAN</span>
                    </div>
                    <div class="text-center col-12 col-sm-4 col-md-4 col-lg-4">
                        <a href="mailto:unMaxdeFormation@afpa.fr">Contactez nous</a>
                    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-4 text-end">
                        <i class="bi bi-facebook"></i>
                        <i class="bi bi-twitter"></i>
                        <i class="bi bi-instagram"></i>
                        <i class="bi bi-linkedin"></i>
                    </div>

            </div>
        </div>  
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>