<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="../CSS/connexion.css">
    <title>Connexion</title>
</head>

<body>
    <div class="wrapper">
        <div class="logo"> <img src="/img/logoBis.jpg" alt=""> </div>
        <div class="text-center mt-4 name"> Menuiz Man </div>
        <form class="p-3 mt-3" type="submit" method="post">
            <div class="form-field d-flex align-items-center">
                <span class="far fa-user"></span> <input type="text" name="userName" id="userName" placeholder="Identifiant">
            </div>
            <div class="form-field d-flex align-items-center"> <span class="fas fa-key"></span>
             <input type="password" name="password" id="password" placeholder="Mot de passe"> </div>
            <button class="btn mt-3">Connexion</button>
            <input type="hidden" name="action" value="connexionMAJ">
        </form>

        <div><?php echo $msg ?></div>

    </div>

</body>

</html>>