<div class="text-center start">
    <h2>Formulaire de Recherche </h2>
</div>

<div class="formulaireChreChre text-center">
<form action="">


    <div class="container">
        <div class="row">

            <div class="col-12 text-center mb-8">
                <label for="nom" >Numéro de Client</label>
                </br>
                <input type="text" id="numCli" class="w-25">
            </div>

            <div class="col-6 text-center mb-5">
                <label for="nom" >Prénom du Client</label>
                </br>
                <input type="text" id="prenomCli" class="w-100" >
            </div>

            <div class="col-6 text-center">
                <label for="nom" >Nom du Client</label>
                </br>
                <input type="text" id="nomCli" class="w-100">
            </div>



            
            <div class="col-12 text-start mb-2">
                <label for="nom" >Adresse</label>
                </br>
                <input type="text" id="adressCli" class="w-100">
            </div>
            <div class="col-3 text-start">
                <label for="nom" >Code Postal</label>
                </br>
                <input type="text" id="cpCli" class="w-50">
            </div>
            <div class="col-9 text-end mb-3">
                <label for="nom" class="text-start" >Ville</label>
                </br>
                <input type="text" id="villeCli" class="w-75">
            </div>

            <div class="col-12 text-center mb-3">
                <label for="nom" >Numéro de téléphone</label>
                </br>
                <input type="text"  id="telCli" class="w-25">
            </div>
            <div class="col-6 text-center">
                <label for="nom" >Numéro de Commande</label>
                </br>
                <input type="text" id="numCmdCli" class="w-100">
            </div>
            <div class="col-6 text-center">
                <label for="nom" >Numéro de Facture</label>
                </br>
                <input type="text" id="numFactCli" class="w-100 mb-3">
            </div>

        </div>
        <div class="col-12 text-center mb-8 last">
            <label for="nom" >Numéro de Ticket</label>
            </br>
            <input type="text" id="numTickCli" class="w-25">
        </div>

        <div class="container">
            <div class="row">
                <div class="text-start col-6"><input type="reset" value="Reset" class="btn btn-danger w-50"></input></div>

                <div class="text-end col-6"><button type="button" id="valider" class="btn btn-success w-50 last">Rechercher</button></div>
            </div>
        </div>
        </form>
    </div>

    <script src="../Script/script_recherche.js"></script>