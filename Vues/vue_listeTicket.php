<div class="listeTicket text-center listticket">

    <h1>Liste Ticket</h1>
    <?php
if(strlen($msg)>0){  ?>
 
    <div class="alert alert-success text-center" role="alert">
    <?php echo $msg ?>
</div> <?php } ?>

    <table class="table text-center">

        <thead>
            <tr class="table-primary">

                <th>Numéro du Ticket</th>
                <th>Date de création du ticket</th>
                <th>Motif du ticket</th>
                <th>Numero de commande</th>
                <th>Numero dossier</th>
                <th>Details</th>
            </tr>
        </thead>

        <tbody>
            <?php
            
            foreach ($tListeTicket as $key=>$value) { ?>
                <tr>
                    <td><?php echo $value["idTicket"]; ?></td>
                    <td><?php echo $value["dateTicket"]; ?></td>
                    <td><?php echo $value["motifTicket"]; ?></td>
                    <td><?php echo $value["numCommande"]; ?></td>
                    <td><?php echo $value["numDossier"]; ?></td>
                    <td><form action="" method="post">

                    <button class="bi bi-info-circle-fill" type="submit">
                        </button>
                        <input type="hidden" name="action" value="infoTicket">
                        <input type="hidden" name="idTicket" value="<?php echo $value["idTicket"]; ?> "></form>
                    </td>
                </tr>
            <?php }

            ?>
        </tbody>

    </table>


</div>