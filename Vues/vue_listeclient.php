
<div id="listeTicket" class="text-center">
  </br>
  <h1 class="text-center">Liste des Clients</h1>


  <table class="table">
    <thead>
      <tr>
        <th scope="col" class="col12">n° Client</th>
        <th scope="col">Nom</th>
        <th scope="col">Prénom</th>
        <th scope="col">Adresse</th>
        <th scope="col">Ville</th>
        <th scope="col">Code postal</th>
        <th scope="col">Afficher les commandes </th>
      </tr>
    </thead>
    <tbody>
      <?php  for ($i = 0; $i < count($tVille); $i++)  { ?>
        <tr scope="row">
          <td><?php echo htmlspecialchars($tVille[$i]['idClient']); ?></td>
          <td><?php echo htmlspecialchars($tVille[$i]['nomClient']); ?></td>
          <td><?php echo htmlspecialchars($tVille[$i]['prenomClient']); ?></td>
          <td><?php echo htmlspecialchars($tVille[$i]['adresseClient']); ?></td>
          <td><?php echo htmlspecialchars($tVille[$i]['nomVille']); ?></td>
          <td><?php echo htmlspecialchars($tVille[$i]['cpVille']); ?></td>
          <form method="post" action="">
            <td><button type="submit" class="btn btn-success">Commandes</button></td>
            <input type="hidden" name="action" value="AfficherCommandes">
            <input type="hidden" name="idClient" value="<?php echo $tVille[$i]['idClient'] ?>">
          </form>
        </tr>
      <?php  }?>
    </tbody>
  </table>
</div>
