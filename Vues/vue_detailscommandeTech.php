<h1 class="text-center">Détails de la commande</h1>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Libellé produit</th>
      <th scope="col">Référence produit</th>
      <th scope="col">Prix du produit</th>
      <th scope="col">Quantité commandée</th>
      <th scope="col">N° de Garantie</th>



    </tr>
  </thead>
  <tbody>

    <?php
    for ($i = 0; $i < count($tCommande); $i++) {
    ?>

      <tr>
        <td><?php echo htmlspecialchars($tCommande[$i]['nomProduit']); ?></td>
        <td><?php echo htmlspecialchars($tCommande[$i]['refProduit']); ?></td>
        <td><?php echo htmlspecialchars($tCommande[$i]['prixProduit']); ?></td>
        <td><?php echo htmlspecialchars($tCommande[$i]['quantiteProduit']); ?></td>
        <td><?php echo htmlspecialchars($tCommande[$i]['idGarantie']); ?></td>
   



        </form>
      </tr>
    <?php } ?>
  </tbody>
  <div class="text-center">
    <?php if (count($tCommande) < 1) {
      echo "<td> Aucun article </td>";
    }
    ?>
</table>