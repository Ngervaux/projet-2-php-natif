<body>
    <div class="container-fluid">
        <div class="row menu" id="menu">
            <div class="menugauche" id="menugauche">


                <div class="col-12 test1">
                    <br>
                    <form method="post">
                        <button class="btn mt-3 msg"><i class="bi bi-ticket-detailed msg "></br>Mes tickets</i>
                </div>
                <input type="hidden" type="submit" name="action" value="listeTickets" />
                </form>

                <hr>

                <div class="col-12">
                    <form method="post">
                        <button class="btn mt-3 msg"><i class="bi bi-person-heart msg">
                                </br>Liste client</i></button>
                </div>
                <input type="hidden" type="submit" name="action" value="listeClientTech" />
                </form>

                <hr>

                <div class="col-12">
                    <form method="post">
                        <button class="btn mt-3"><i class="bi bi-basket msg"></br>Liste </br> commandes</i>
                        </button>
                </div>
                <input type="hidden" type="submit" name="action" value="listeCommandeTech" />
                </form>

                <hr>
                <div class="col-12 recherche">
                    <form method="post">
                        <button class="btn mt-3 recherche"><i class="bi bi-search recherche">
                                </br>Recherche</i></button>
                </div>
                <br>
                <br>
                <input type="hidden" type="submit" name="action" value="Recherche" />
                </form>
                

            </div>
        </div>
    </div>
    <div class="menuMobile">
        <div class="container-fluid">
            <div class="row" id="menu">
            



                <div class="col-3 text-center ">
                    <form method="post">
                        <button class="btn mt-3 msg"><i class="bi bi-ticket-detailed msg"></br>Mes tickets</i>
                </div>
                <input type="hidden" type="submit" name="action" value="listeTickets" />
                </form>



                <div class="col-3 text-center ">
                    <form method="post">
                        <button class="btn mt-3 msg"><i class="bi bi-person-heart msg">
                                </br>Liste client</i></button>
                </div>
                <input type="hidden" type="submit" name="action" value="listeClientTech" />
                </form>

                <div class="col-3 text-center">
                    <form method="post">
                        <button class="btn mt-3"><i class="bi bi-basket msg"></br>Liste commandes</i>
                        </button>
                </div>
                <input type="hidden" type="submit" name="action" value="listeCommandeTech" />
                </form>

                <div class="col-3 text-center">
                    <form method="post">
                        <button class="btn mt-3 msg"><i class="bi bi-search msg">
                                </br>Recherche</i></button>
                </div>
                <input type="hidden" type="submit" name="action" value="Recherche" />
                </form>


            </div>
        </div>
    </div>





    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>

</html>