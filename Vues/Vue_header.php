<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../CSS/style_header.css"> <!-- CSS HEADER -->
    <link rel="stylesheet" href="../CSS/listecmd.css"> <!-- CSS LSITE COMMANDE -->
    <link rel="stylesheet" href="../CSS/style_footer.css"> <!-- CSS FOOTER -->
    <link rel="stylesheet" href="../CSS/styleformulaire.css"> <!-- CSS FORMULAIRE -->
    <link rel="stylesheet" href="../CSS/style_button.css"> <!-- CSS VUE HOTLINE -->
    <link rel="stylesheet" href="../CSS/listeclient.css">  <!-- CSS VUE LISTECLIENT -->
    <link rel="stylesheet" href="../CSS/styleformulaire.css"> <!-- CSS VUE FORMULAIRE -->
    <link rel="stylesheet" href="../CSS/listeticket.css"> <!-- CSS VUE Ticket -->
    <link rel="stylesheet" href="../CSS/recherche.css">
    <link rel="stylesheet" href="../CSS/style_infoTicket.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Menuiz Man</title>
</head>

<body>
    <header>
        <!-- PARTIE HEADER (LOGO, NOM SERVICE, CONNEXION, NOM EMPLOYE) -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 col-sm-4 col-md-4 col-lg-4 logo text-start">
                    <img class="img-fluid" src="../img/logo.jpg " alt="">
                </div>
                <div class="col-8 col-sm-4 col-md-4 col-lg-4 hotline text-center">
                    <?php echo $_SESSION['role'] ?>
                </div>
                <div class="col-6 col-sm-2 col-md-2 col-lg-2 msg  text-center disconnect">
                    <form method="post">
                        <button class="btn mt-3 msg"> <i class="bi bi-house-door-fill msg">
                                </br>Accueil</i></button>

                        <input type="hidden" type="submit" name="action" value="accueil" />
                    </form>
                </div>
                <div class="col-6 col-sm-2 col-md-2 col-lg-2  text-center disconnect msg">
                    <form method="post">
                        <button class="btn mt-3 msg "><i class="bi bi-person-x-fill text-center msg">
                                </br>Deconnexion</i></button>

                        <input type="hidden" type="submit" name="action" value="deconnexion" />
                    </form>
                </div>
                
                    
                        
                </div></i>
<div>
</div>
            </div>
        </div>
        </div>
        <div class="text-center bienvenue">
            <?php
            echo 'Bienvenue ' . strtoupper($_SESSION['nom']) ." " .   ucfirst($_SESSION['prenom']);
            ?>


        </div>

    </header>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>