<form action="index.php" method="post">
    <button type="submit" class="btn btn-primary btn-lg">Ajouter un salarié</button>
    <input type="hidden" name="action" value="addSalarie">
</form>

<?php
if (strlen($msg) > 0) {
    if ($flag == 0) {?>
    <div class="alert alert-success text-center" role="alert">
        <?php echo $msg; ?>
    </div>
<?php } else { ?>
    <div class="alert alert-danger text-center" role="alert">
        <?php echo $msg; ?>
    </div>
<?php }} ?>


<div class="container-fluid" id="affAdmin">
    <?php
    if (count($tSalarie)) {
        $compteur = 0;
        for ($i = 0; $i < count($tSalarie); $i++) {
            if ($compteur % 4 == 0) {
                echo "<div class='row'>";
            }
    ?>
            <div class="card col-3 col-md pb-1" style="width: 18rem">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $tSalarie[$i]['nomSalarie']; ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?php echo $tSalarie[$i]['prenomSalarie']; ?></h6>
                    <h6 class="card-subtitle mb-2 text-muted"><?php echo $tSalarie[$i]['roleSalarie']; ?></h6>
                    <form action="index.php" method="post">
                        <button type="submit" class="card btn btn-outline-success">Modifier</button>
                        <input type="hidden" name="action" value="updateSalarie">

                        <input type="hidden" name="idSalarie" value="<?php echo $tSalarie[$i]['idSalarie']; ?>" />
                        <input type="hidden" name="nomSalarie" value="<?php echo $tSalarie[$i]['nomSalarie']; ?>" />
                        <input type="hidden" name="prenomSalarie" value="<?php echo $tSalarie[$i]['prenomSalarie']; ?>" />
                        <input type="hidden" name="roleSalarie" value="<?php echo $tSalarie[$i]['roleSalarie']; ?>" />
                    </form>
                    <form action="index.php" method="post">
                        <button type="submit" class="card btn btn-outline-danger">Supprimer</button>
                        <input type="hidden" name="action" value="deleteSalarie">
                        <input type="hidden" name="idSalarie" value="<?php echo $tSalarie[$i]['idSalarie']; ?>" />
                        <input type="hidden" name="nomSalarie" value="<?php echo $tSalarie[$i]['nomSalarie']; ?>" />
                        <input type="hidden" name="prenomSalarie" value="<?php echo $tSalarie[$i]['prenomSalarie']; ?>" />
                    </form>
                </div>
            </div>
            <?php
            if ($compteur % 4 == 3) {
                echo "</div>";
            }
            $compteur++;
            ?>
        <?php ;
        } ?>
    <?php  } else { ?>
        <div class="alert alert-danger text-center" role="alert">
            <?php echo "Aucun salarié en BDD" ?>
        </div>
    <?php  }

    ?>
</div>