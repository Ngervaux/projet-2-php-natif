<?php
if(strlen($msg)>0){  ?>
 
    <div class="alert alert-danger text-center" role="alert">
    <?php echo $msg ?>
</div> <?php } ?>

<div class="formulaireAjout">

        <form action="" method="post" id="formulaire">

            <fieldset>

                <h1>Création Nouveaux Tickets</h1>

                <div class="motifTicket">
                    <label for="motifTicket">Motif du Ticket :</label>
                    <input type="text" name="motifTicket" id="motifTicket" value="<?php echo $motifTicket; ?>">
                </div>

                <div class="numCommande">
                    <label for="numCommande">N° de commande :</label>
                    <input type="text" name="numCommande" id="numCommande" value="<?php echo $numCommande; ?>">
                </div>

                <div class="numDossier">
                    <label for="numDossier">N° de Dossier :</label>
                    <input type="text" name="numDossier" id="numDossier" value="<?php echo $numDossier; ?>">
                </div>

                <div class="send">

                    <input class="btn btn-success" type="button" value="Créer" id="addTicket">
                    </input>
                    <input type="hidden" name="action" value="CreationTicketMaj">
                </div>
            </fieldset>


        </form>
</div>

<script src="/Script/controleAddTicket.js"></script>