<?php

class Ticket
{

    protected $dateTicket;
    protected $motifTicket;
    protected $numCommande;
    protected $numDossier;

    /**
     * 
     * @param idTicket numero du ticket
     * @param dateTicket date de creation du ticket
     * @param motifTicket motif du ticket
     * @param numCommande numero de la commande concernant le ticket
     * @param numDossier numero du dossier concernant le ticket
     * 
     */

    public function __construct(string $dateTicket, string $motifTicket, int $numCommande, int $numDossier)
    {
        $this->setDateTicket($dateTicket);
        $this->setMotifTicket($motifTicket);
        $this->setNumCommande($numCommande);
        $this->setNumDossier($numDossier);
        
    }

    // Getter et setteur

    public function getDateTicket()
    {
        return $this->dateTicket;
    }

    public function setDateTicket($dateTicket)
    {
        $this->dateTicket = $dateTicket;
    }

    public function getMotifTicket()
    {
        return $this->motifTicket;
    }

    public function setMotifTicket($motifTicket)
    {
        $this->motifTicket = $motifTicket;
    }

    public function getNumCommande()
    {
        return $this->numCommande;
    }

    public function setNumCommande($numCommande)
    {
        $this->numCommande = $numCommande;
    }

    public function getNumDossier()
    {
        return $this->numDossier;
    }

    public function setNumDossier($numDossier)
    {
        $this->numDossier = $numDossier;
    }

    public function __toString()
    {
        return "Le Ticket numero : ". $this->idTicket." concernant la commande : " . $this->numCommande . " est creer. ";
    }
}
