<?php
require_once("Modele/DbSav.class.php");
require_once("Modele/ManagerTicketSAVException.class.php");
require_once("Modele/TicketSAV.class.php");
class ManagerTicketSAV
{

    public static function getListTicket(int $enregistrement = PDO::FETCH_ASSOC)
    {

        $sql = "SELECT * FROM ticket_sav";
        $resultat = Connexion::roleConnexion($_SESSION["role"])->query($sql);

        if ($enregistrement === PDO::FETCH_CLASS) {
            require_once("Ticket.classe.php");
            $resultat->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "Ticket", array("idTicket", "dateTicket", "motifTicket", "dateClotureTicket", "etatTicket", "idDiagnostique", "numCommande"));
            $tData = $resultat->fetchAll();
        } else {
            $tData = $resultat->fetchAll($enregistrement);
        }

        $resultat->closeCursor();

        return $tData;
    }

    public static function getInfoTicket(int $idTicket)
    {

        $sql = "SELECT t.idTicket, t.motifTicket,t.dateTicket,t.numDossier ,c.numCommande, cl.nomClient, cl.prenomClient FROM `ticket_sav` as t
        join commande as c
        ON t.numCommande = c.numCommande
        join  client as cl
        on c.idClient = cl.idClient
                WHERE idTicket = ?";
        $resultatTicket = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);
        $resultatTicket->execute(array($idTicket));

        $tData = $resultatTicket->fetchAll();
        $resultatTicket->closeCursor();

        if ($tData == false) throw new ManagerTicketSAVException("ID ticket inconnue");
        return $tData;
    }

    public static function addTicket(Ticket $ticket){

        $sql = "INSERT INTO `ticket_sav`(`dateTicket`, `motifTicket`, `numCommande`, `numDossier`)
                VALUES (:dateTicket,:motifTicket,:numCommande,:numDossier)";

        $resultatTicket = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);
        $resultatTicket->execute(array(':dateTicket'=>$ticket->getDateTicket(),
                                        ':motifTicket'=>$ticket->getMotifTicket(),
                                        'numCommande'=>$ticket->getNumCommande(),
                                        ':numDossier'=>$ticket->getNumDossier()));

        $resultat = $resultatTicket->rowCount();

        $resultatTicket->closeCursor();

        return $resultat;
    }

    public static function checkNumCommande($numCommande){
        $sql = "SELECT COUNT(numCommande) FROM commande WHERE numCommande = :numCommande";

        $resultSet = Connexion::roleConnexion($_SESSION['role'])->prepare($sql);
        $resultSet->execute(array(':numCommande'=>$numCommande));

        $result = $resultSet->fetch();
        $resultSet->closeCursor();
        $nb = $result[0];
        return $nb;
    }

    public static function ajoutGerer($idSalarie){
        $sql = "INSERT INTO gerer VALUES ((SELECT MAX(idTicket) FROM ticket_sav), :idSalarie)";

        $resultSet = Connexion::roleConnexion($_SESSION['role'])->prepare($sql);
        $resultSet->execute(array(':idSalarie'=>$idSalarie));
        $nb = $resultSet->rowCount();
        $resultSet->closeCursor();


    }

}
