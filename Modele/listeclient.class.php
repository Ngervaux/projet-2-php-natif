<?php

    class ListeClient 
    {
        public static function getListClient(int $id)
        {
                  //Requête :
        $sql = "SELECT * from client as cli JOIN commande as c ON cli.idClient = c.idClient where cli.idClient = :id ";

        //Exécuter la requête
        $resultset = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);

        //Mettre les résultats dans un tableau
        // $resultset->setFetchMode(PDO::FETCH_ASSOC);

        $resultset->execute(array(
            ':id' => $id
        ));

        $tresult = $resultset->fetchAll(PDO::FETCH_ASSOC);

        //Fermer le curseur
        $resultset->closeCursor();

        
        //Détruire la connexion :
        DbSavClass::disconnect();

        //Retourner le tableau
        return $tresult;
        }

        public static function getVilleClient()
        {
                  //Requête :
        $sql = "SELECT c.idClient, c.nomClient, c.prenomClient, c.adresseClient, c.idVille, v.nomVille, v.cpVille FROM client as c JOIN ville as v ON c.idVille = v.idVille;";

        //Exécuter la requête
        $resultset = Connexion::roleConnexion($_SESSION["role"])->query($sql);

        //Mettre les résultats dans un tableau
        // $resultset->setFetchMode(PDO::FETCH_ASSOC);

      
        $tresult = $resultset->fetchAll(PDO::FETCH_ASSOC);

        //Fermer le curseur
        $resultset->closeCursor();

        
        //Détruire la connexion :
        DbSavClass::disconnect();

        //Retourner le tableau
        return $tresult;
        }
    }

    