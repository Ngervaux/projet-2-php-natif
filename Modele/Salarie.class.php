<?php

class Salarie
{
    private $id;
    private $nom;
    private $prenom;
    private $role;
    private $utilisateur;
    private $motdepasse;

    public function __construct(int $id, string $nom, string $prenom, string $role, string $utilisateur, string $motdepasse)
    {
        $this->setId($id);
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setRole($role);
        $this->setUtilisateur($utilisateur);
        $this->setMotdepasse($motdepasse);
    }

    //Setters :

    private function setId($id)
    {
        $this->id = $id;
    }

    private function setNom($nom)
    {
        $this->nom = $nom;
    }

    private function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    private function setRole($role)
    {
        $this->role = $role;
    }

    private function setUtilisateur($utilisateur){
        $this->utilisateur = $utilisateur;
    }

    private function setMotdepasse($motdepasse){
        $this->motdepasse = $motdepasse;
    }

    //Getters :

    public function getId()
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getMdP()
    {
        return $this->motdepasse;
    }

    public function getUtil()
    {
        return $this->utilisateur;
    }

    //Affichage :
    public function __toString()
    {
        return $this->nom . " " . $this->prenom . " (id : " . $this->id . ") " . $this->role;
    }
}
