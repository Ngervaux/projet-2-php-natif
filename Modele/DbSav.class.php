<?php

class DbSavClass
{
//Variable static
private static $connexion;


//Fonction de connexion
// fonction de connexion à la BDD
private static function connect($user, $password) {
    // Récupérer les paramètres de la BDD avec les sections
    $tParam = parse_ini_file("param/param.ini", true); 
    
    // Crée dynamiquement les variables équivalentes 
    // aux clés de tParam pour la section "BDD"
    extract($tParam["BDD"]); 

    $dsn = "mysql:host=" . $hote 
           . "; port=" . $port
           . "; dbname=" . $dbname . "; charset=utf8";
    
    try {
        $mysqlPDO = new PDO($dsn, $user, $password,
                        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    } catch(Exception $e) { 
        // en cas erreur on affiche un message et on arrete tout
        die('<h1>Erreur de connexion : </h1>' . $e->getMessage());
    }
    
    DbSavClass::$connexion = $mysqlPDO;
    
    return DbSavClass::$connexion;
}

// fonction de 'déconnexion' de la BDD
public static function disconnect(){
    DbSavClass::$connexion = null;
}

// Pattern singleton
public static function getConnexion($user, $password) {
    if (DbSavClass::$connexion != null) {
        return DbSavClass::$connexion;
    } else {
        return DbSavClass::connect($user, $password);
    }
}

}