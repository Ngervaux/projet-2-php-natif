<?php

class Connexion
{
    /**
     * Il prend un nom d'utilisateur et un mot de passe, vérifie si le mot de passe est correct, et si
     * c'est le cas, il renvoie les informations de l'utilisateur
     * 
     * @param user l'identifiant
     * @param pwd le mot de passe
     * 
     * @return Le résultat de la requête.
     */
    public static function verifConnexion($user, $pwd)
    {
        if (!self::verifHash($user, $pwd)) {
            $tResult = [];
        } else {
            $sql = "SELECT IdSalarie,nomSalarie,prenomSalarie,roleSalarie FROM salarie WHERE utilisateurSalarie = '" . $user . "'";


            $tResultSet = self::roleConnexion('Verif')->query($sql);

            $tResult = $tResultSet->fetchAll();
        }



        return $tResult;
    }

   /**
    * Il prend un rôle en tant que paramètre, analyse un fichier, extrait l'utilisateur et le mot de
    * passe et renvoie une connexion.
    * 
    * @param role le rôle de l'utilisateur (administrateur, utilisateur, etc.)
    */
    public static function roleConnexion($role)
    {
        $tParam = parse_ini_file("param/param.ini", true);


        extract($tParam[$role]);

        return DbSavClass::getConnexion($user, $password);
    }


    /**
     * Il vérifie si le mot de passe est correct
     * 
     * @param user l'identifiant
     * @param pwd le mot de passe à vérifier
     * 
     * @return bool Le résultat de la requête.
     */
    public static function verifHash($user, $pwd): bool
    {
        $sql = "SELECT motdepasseSalarie FROM salarie WHERE utilisateurSalarie = :user";
        $resultSet = DbSavClass::getConnexion('check', 'role')->prepare($sql);
        $resultSet->execute(array(':user' => $user));
        $tres = $resultSet->fetch();
        if (empty($tres)) {
            throw new Exception('Erreur login ou mot de passe');
        } elseif (password_verify($pwd, $tres[0]) == 1) {
            return true;
        } else {
            throw new Exception('Erreur login ou mot de passe');
        }
    }
}
