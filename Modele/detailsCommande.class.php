<?php

    class DetailsCommandes 
    {
        public static function getDetailsCommande($id)
        {
                  //Requête :
        $sql = "SELECT * FROM comprendre as c JOIN produit as p ON p.refProduit = c.refProduit where numCommande = :id";

        //Exécuter la requête
        $resultset = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);

        //Mettre les résultats dans un tableau
        // $resultset->setFetchMode(PDO::FETCH_ASSOC);

        $resultset->execute(array(
            ':id' => $id
        ));

        $tresult = $resultset->fetchAll(PDO::FETCH_ASSOC);

        //Fermer le curseur
        $resultset->closeCursor();

        
        //Détruire la connexion :
        DbSavClass::disconnect();

        //Retourner le tableau
        return $tresult;
        }


        public static function getNomproduit($id)
        {
                  //Requête :
        $sql = "SELECT * FROM produit where refProduit = :id";

        //Exécuter la requête
        $resultset = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);

        //Mettre les résultats dans un tableau
        // $resultset->setFetchMode(PDO::FETCH_ASSOC);

        $resultset->execute(array(
            ':id' => $id
        ));

        $tresult = $resultset->fetchAll(PDO::FETCH_ASSOC);

        //Fermer le curseur
        $resultset->closeCursor();

        
        //Détruire la connexion :
        DbSavClass::disconnect();

        //Retourner le tableau
        return $tresult;
        }
    }
