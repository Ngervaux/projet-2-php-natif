<?php
class Admin
{

   /**
    * Il obtient une liste d'employés de la base de données
    * 
    * @return Un tableau de tableaux associatifs.
    */
    public static function getListeSalaries()
    {
        //Requête :
        $sql = "SELECT idSalarie,nomSalarie,prenomSalarie,roleSalarie FROM salarie WHERE roleSalarie NOT IN ('Admin','Archive') ";

        //Exécuter la requête
        $resultset = Connexion::roleConnexion($_SESSION["role"])->query($sql);

        //Mettre les résultats dans un tableau
        $tresult = $resultset->fetchAll(PDO::FETCH_ASSOC);

        //Fermer le curseur
        $resultset->closeCursor();

        //Détruire la connexion :
        DbSavClass::disconnect();

        //Retourner le tableau
        return $tresult;
    }

    /**
     * Insert un nouvel utilisateur dans la BDD
     * 
     * @param Utilisateur utilisateur l'objet qui contient les données à insérer dans la base de
     * données
     */
    public static function addSalarie(Salarie $salarie): bool
    {
        $res = true;
        //Requête :
        $sql = "INSERT INTO salarie (idSalarie,nomSalarie,prenomSalarie,roleSalarie,utilisateurSalarie,motdepasseSalarie) VALUES (?,?,?,?,?,?)";


        // Préparer le ResultSet à partir de la connexion :
        $resultset = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);

        $res = $resultset->execute(array(
            $salarie->getId(),
            $salarie->getNom(),
            $salarie->getPrenom(),
            $salarie->getRole(),
            $salarie->getUtil(),
            $salarie->getMdP()
        ));

        //Fermer le curseur
        $resultset->closeCursor();
        //Détruit la connexion
        DbSavClass::disconnect();


        return $res;
    }

   
    /**
     * Il supprime une ligne de la table "salarie" de la base de données où l'idSalarie est égal à
     * l'idSalarie passé en paramètre.
     * 
     * @param int idSalarie l'identifiant de l'employé à supprimer
     * 
     * @return int Le nombre de lignes affectées par la requête.
     */
    public static function delSalarieById(int $idSalarie): int
    {
        
        //Requete :
        $sql = "DELETE FROM salarie WHERE idSalarie=?";

        //Préparer le Resultset
        $resultset = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);

        $res = $resultset->execute(array($idSalarie));

        $nombre = $resultset->rowCount();

        //Fermer le curseur
        $resultset->closeCursor();
        //Détruit la connexion
        DbSavClass::disconnect();

        return $nombre;
    }

    /**
     * Il met à jour une ligne dans la base de données
     * 
     * @param int id entier
     * @param string nom 
     * @param string prenom 
     * @param string roleSal le rôle du salarié
     * 
     * @return int Le nombre de lignes affectées par la requête.
     */
    public static function updateSalarieById(int $id, string $nom, string $prenom, string $roleSal): int
    {
        $sql = "UPDATE salarie SET nomSalarie=:nom, prenomSalarie=:prenom, roleSalarie=:roleSal WHERE idSalarie=:id";
        
        if(strlen($nom)==0 || strlen($prenom)==0){
            $nombre =0;
         
        }
        else{
        //Préparer le resultset
        $resultset = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);

        $res = $resultset->execute(array(
            ':nom' => $nom,
            ':prenom' => $prenom,
            ':roleSal' => $roleSal,
            ':id' => $id
        ));

        $nombre = $resultset->rowCount();

        //Fermer le curseur
        $resultset->closeCursor();
        //Détruit la connexion
        DbSavClass::disconnect();
        }
        return $nombre;
    }


/**
 * Il renvoie le nombre de tickets liés à un employé donné.
 * 
 * @param int id l'identifiant de l'employé
 * 
 * @return array Le nombre de tickets attribués à un employé donné.
 */
    public static function verifLienSalarieTicket(int $id): array {
        $sql ="SELECT COUNT(idTicket) FROM `gerer` WHERE idSalarie =:id";

        //Préparer le resultset
        $resultset = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);

        $resultset->execute(array(':id' =>$id));
        $res = $resultset->fetch();
        //Fermer le curseur
        $resultset->closeCursor();
        //Détruit la connexion
        DbSavClass::disconnect();

        return $res;
    }

  /**
   * Vérifie si l'identifiant utilisateur est déjà présent en BDD
   * 
   * @param string identifiant l'identifiant à vérifier
   * 
   * @return int Le nombre de lignes de la table qui correspondent aux critères.
   */
    public static function verifUtilisateurDansTable(string $identifiant): int{
        $sql="SELECT COUNT(nomSalarie) FROM salarie WHERE utilisateurSalarie LIKE :utilSalarie";
    
        $resultset = Connexion::roleConnexion($_SESSION["role"])->prepare($sql);
        $resultset->execute(array('utilSalarie' =>$identifiant));
        $res = $resultset->fetch();
        $resultset->closeCursor();
        DbSavClass::disconnect();
        $nb = $res[0];
        return $nb;
        }


        //Les fonctions dont je ne me sers plus :

/**
 * Il vérifie si l'utilisateur est déjà dans la base de données, si l'utilisateur est déjà dans la base
 * de données, il renvoie 0, si l'utilisateur n'est pas dans la base de données, il vérifie si le nom,
 * le prénom, le nom d'utilisateur et le mot de passe de l'utilisateur ne sont pas vides, s'ils ne sont
 * pas vides, il vérifie si le nom, le prénom et le nom d'utilisateur de l'utilisateur sont valides,
 * s'ils sont valides, il renvoie 1, s'ils ne sont pas valides, il renvoie 0
 * 
 * @param string nom corde
 * @param string prenom corde
 * @param string identifiant l'identifiant
 * @param string mdp le mot de passe
 */
    // public static function verifAjoutSalarie(string $nom, string $prenom, string $identifiant, string $mdp): int{
    //     $tresult = self::verifUtilisateurDansTable($identifiant);
    //     $nombreTrouve = $tresult[0];       
    
        
    //     if(strlen($nom)==0 || strlen($prenom)==0 || strlen($identifiant)==0 || strlen($mdp)==0)
    //     {
    //         $nb = 0;
    //     }
    //     elseif ($nombreTrouve !=0)
    //     {
    //         $nb =0;
    //     }
    //     elseif (!self::isRegex($nom) || !self::isRegex($prenom) || !self::isRegex($identifiant))
    //     {
    //         $nb =0;
    //     }
    //     else
    //     {
    //         $nb = 1;
    //     }
        
        
    //     return $nb;
    // }



/**
 * Elle renvoie true si la chaîne ne contient que des lettres, des espaces, des traits d'union et des
 * apostrophes.
 * 
 * @param string chrchr Chaîne à vérifier.
 * 
 * @return une valeur booléenne.
 */
    // public static function isRegex(string $chrchr){
    //     $regex = '/^[a-zA-Z\u00C0-\u00FF\s\'\-]*$/';
    //     return preg_match($regex,$chrchr);
    // }
}
